from setuptools import setup

setup(
    name='cloud_manager',
    version='0.0',
    description='tools for managing cloud servers',
    url='http://github.com/akarasz/cloud_manager',
    author='András Kárász',
    author_email='karaszandris@gmail.com',
    license='BSD',
    scripts=['bin/init_minion.py'],
    install_requires=[
        'sh'
    ],
    zip_safe=False)
