#!/usr/bin/python3

from configparser import ConfigParser
from os.path import expanduser

import re
import sh
import sys
import time

IP = sys.argv[1]
NAME = sys.argv[2]

CONFIG_FILE = '~/.cloud_manager.conf'

def confirm(msg, default=''):
    if default == 'y':
        prompt = 'Y/n'
    elif default == 'n':
        prompt = 'y/N'
    else:
        default = ''
        prompt = 'y/n'

    while True:
        answer = input(msg + ' [' + prompt +'] ').strip()
        
        if answer == '':
            answer = default

        if re.search(r'^y', answer, re.IGNORECASE):
            return True
        elif re.search(r'^n', answer, re.IGNORECASE):
            return False

def print_line(line):
    print(line.rstrip('\n'))

# prepare sh commands
salt_key = sh.salt_key.bake('--no-color', '-y')
ssh = sh.ssh.bake('-o', 'StrictHostKeyChecking=no', _out=print_line,
        _out_bufsize=1)
ungrep = sh.grep.bake('-v')

# init config parser
config = ConfigParser()
config.read(expanduser(CONFIG_FILE))

print('*** CHECK IF THERE IS ALREADY A KEY WITH THIS DOMAIN')
keys = salt_key('-L')

if re.search('^' + NAME + '$', str(keys), re.MULTILINE):
    print('WARNING: a key already exists for this domain!')
    if confirm('Do you want to delete the current key?'):
        salt_key('-d', NAME)
    else:
        sys.exit(1)

print('*** PREPARE REMOTE SIDE')
ssh = ssh.bake('root@' + IP)
ssh('echo "' + config['default']['salt-master'] + ' salt" >> /etc/hosts')
ssh('apt-get update; apt-get -y install salt-minion')
ssh('systemctl enable salt-minion; systemctl restart salt-minion')

print('*** ACCEPTIONG REMOTE KEY')
remaining_time = 10
while remaining_time > 0:
    time.sleep(1)

    keys = salt_key('-l', 'una')
    if re.search('^' + NAME + '$', str(keys), re.MULTILINE):
        salt_key('-a', NAME)
        break

    remaining_time = remaining_time - 1

if remaining_time == 0:
    print('Some shit happened - didn\'t receive key from remote.')
    sys.exit(1)
else:
    print('Key for ' + NAME + ' accepted.')

print('*** OFFER HIGHSTATE')
if confirm('Do you want to bring up ' + NAME + '?'):
    sh.salt('-v', '-t', '60', NAME, 'state.highstate', _out=print_line, _out_bufsize=1)

print('done.')
